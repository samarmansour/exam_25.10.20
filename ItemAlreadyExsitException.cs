﻿using System;
using System.Runtime.Serialization;

namespace SecretAgent
{
    [Serializable]
    internal class ItemAlreadyExsitException : Exception
    {
        public ItemAlreadyExsitException()
        {
        }

        public ItemAlreadyExsitException(string message) : base(message)
        {
        }

        public ItemAlreadyExsitException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ItemAlreadyExsitException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}