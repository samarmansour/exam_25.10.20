﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessWordGame
{
    class Game
    {
        private string[] words = {"apple", "coconut", "dragonfruit", "landscape", "glasses" };

        /// <summary>
        /// picks word from words array, and return it
        /// </summary>
        /// <returns></returns>
        public string RandomWord()
        {
            Random rnd = new Random();
            int index = rnd.Next(words.Length);
            return words[index];
        }

        /// <summary>
        /// set up and display game board 
        /// </summary>
        public void PrintGameBoard(char[] spaces)
        {
            for (int i = 0; i < spaces.Length; i++)
            {
                Console.Write("_ ");
            }
        }

        public bool playGame(char[] letters, char[] spaces)
        {
            bool wonGame = false;
            string guess = "";
            int count = 0;
            Console.WriteLine("=======================Guess Word Game=============================");
            Console.Write("Are you lucky enouph to guess the word!\nLet's Play ");
            PrintGameBoard(spaces);
            Console.Write("Enter letter: ");

            while (spaces.Contains('\0'))
            {
                guess = Console.ReadLine();
                if (guess == "" || guess == null)
                    throw new ArgumentNullException("Null input, An Exception occured!!");
                char charGuess = guess.ToLower().ToCharArray()[0];
                if (CheckGuess(letters, charGuess))
                {
                    for (int i = 0; i < letters.Length; i++)
                    {
                        if(letters[i] == charGuess)
                        {
                            spaces[i] = letters[i];
                            letters[i] = '_';
                        }
                    }
                    if (spaces.Contains('\0'))
                    {
                        Console.Clear();
                        Console.WriteLine("GreatGuess! Choose another letter: ");
                        count++;
                        PrintGameBoard(spaces);
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine("Great Guess! You won! ");
                        wonGame = true;
                        PrintGameBoard(spaces);
                    }
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Try Again ");
                    count++;
                    PrintGameBoard(spaces);
                }
            }




            Console.WriteLine($"Your Guesses: {count}");
            return wonGame;
        }

        public bool CheckGuess(char[] letter, char charGuess)
        {
            return letter.Contains(charGuess);
        }

    }
}
