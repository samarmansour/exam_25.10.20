﻿using System;
using System.Runtime.Serialization;

namespace GuessGameSolution2
{
    [Serializable]
    internal class FileNotExists : Exception
    {
        public FileNotExists()
        {
        }

        public FileNotExists(string message) : base(message)
        {
        }

        public FileNotExists(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected FileNotExists(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}