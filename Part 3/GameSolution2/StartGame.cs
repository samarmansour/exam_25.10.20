﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessGameSolution2
{
    class StartGame
    {
        private List<string> words = new List<string>();
        private Random rnd = new Random();

        public int ReadWordsFromFile()
        {
            string file = "WordsArray.txt";
            if (File.Exists(file) == false)
                throw new FileNotExists("Sorry! connot found you file!");
            StreamReader reader = new StreamReader(file);
            int count = 0;
            for (int i = 0; i < file.Length; i++)
            {
                words[count++] = reader.ReadLine(); // adding the word from file to the list
            }
            reader.Close(); // closeing the stream... I could write with using it'll handle this closure
            return count;
        }

        public string RandomWord(int count)
        {
            int index = Convert.ToInt32(rnd.Next(count));
            string guessWord = words[index];
            return guessWord;
        }

        public void PrintDisplay(string word)
        {
            for (int i = 0; i < word.Length; i++)
            {
                Console.Write("_ ");
            }
        }
        public void OpenGame()
        {
            Console.WriteLine("==================Welsome To Guess Word Game=======================");
            Console.WriteLine("Pick one of the Options:\n 1. New Game\n 2. Exit");
            int num = int.Parse(Console.ReadLine());
            switch (num)
            {
                case 1:
                    Play();
                break;
                case 2:
                    Console.WriteLine("Press any key to Exsit...");
                    Console.ReadLine();
                    break;
                default:
                    throw new InputException("Invalid Input!");
            }
        }
        public void Play()
        {
            int guessLimit = 3;
            int guessCount = 0;
            string choice;

            int count = ReadWordsFromFile();
            string secretWord = RandomWord(count);
            PrintDisplay(secretWord);

            bool outOfGuesses = false;
            while (true && guessLimit < guessCount)
            {
                choice = Console.ReadLine();
                if (choice.ToLower().CompareTo(secretWord.ToLower()) == 0)
                {
                    outOfGuesses = true;
                    break;
                }
                for (int i = 0; i < secretWord.Length; i++)
                {
                    if (i < secretWord.Length && i < choice.Length)
                    {
                        if (secretWord[i] == choice[i])
                        {
                            Console.Write(choice[i]);
                        }
                        else
                        {
                            Console.Write("_ ");
                        }
                    }
                    else
                    {
                        Console.Write("_ ");
                    }
                }
                guessCount++;

            }

            if (outOfGuesses)
            {
                Console.WriteLine("You Lose! (T_T')");
            }
            else
            {
                Console.WriteLine("Congrats! You win (* V *)");
            }
        }
    }
}
