﻿using System;
using System.Runtime.Serialization;

namespace RationalNumber
{
    [Serializable]
    internal class ZeroFractionalPartException : Exception
    {
        public ZeroFractionalPartException()
        {
        }

        public ZeroFractionalPartException(string message) : base(message)
        {
        }

        public ZeroFractionalPartException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ZeroFractionalPartException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}