using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RationalNumber
{
    class Rational : IEquatable<Rational>
    {
        public int P { get; set; }
        public int Q { get; set; }

        public Rational(int p, int q)
        {
            P = p;
            if (q <= 0)
                throw new NegativeException($"Q: {q} cannot be negative or zero!!");
            Q = q;
        }

        public static int SplitFirstPart(double num)
        {
            if (num.ToString().Length == 1)
                return Convert.ToInt32(num);
            string[] numToString = num.ToString().Split('.');
            int firstPart = int.Parse(numToString[0]);
            return firstPart;
        }

        public static int SplitFractionPart(double num)
        {
            if (num.ToString().Length == 1)
                throw new ZeroFractionalPartException($"Your input: {num}, Fractional number cannot be zero!!");
            string[] numToString = num.ToString().Split('.');
            int secPart = int.Parse(numToString[1]);
            return secPart;
        }

        public static bool operator >(Rational number, double number2)
        {
            return number.P * SplitFirstPart(number2) > number.Q * SplitFractionPart(number2);
        }

        public static bool operator <(Rational number, double number2)
        {
            return !(number > number2);
        }

        public static bool operator ==(Rational number, double number2)
        {
            return number.P * SplitFirstPart(number2) == number.Q * SplitFractionPart(number2);
        }

        public static bool operator !=(Rational number, double number2)
        {
            return !(number == number2);
        }

        public static Rational operator +(Rational number, double number2)
        {
            Rational result = new Rational((number.P * SplitFractionPart(number2)) + (number.Q * SplitFirstPart(number2)), number.Q * SplitFractionPart(number2));
            return result;
        }

        public static Rational operator -(Rational number, double number2)
        {
            Rational result = new Rational((number.P * SplitFractionPart(number2)) - (number.Q * SplitFirstPart(number2)), number.Q * SplitFractionPart(number2));
            return result;
        }

        public static Rational operator *(Rational number, double number2)
        {
            Rational result = new Rational(number.P * SplitFirstPart(number2), number.Q * SplitFractionPart(number2));
            return result;
        }

        public bool Equals(Rational other)
        {
            return this == other;
        }

        public int getNumerator()
        {
            return P;
        }

        public int getDenominator()
        {
            return Q;
        }
        public override string ToString()
        {
            return $"Rational Number: {P}/{Q}";
        }

       
    }
}
