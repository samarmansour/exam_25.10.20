﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecretAgent
{
    class MyUniqueList_3
    {
        List<int> list = new List<int>();

        public MyUniqueList_3()
        {

        }

        public bool Add(int item)
        {
            if (item > list.Count)
                throw new IndexOutOfRangeException($"{item} is bigger than list length");
            if (list.Contains(item))
            {
                throw new ItemAlreadyExsitException($"Sorry! connat add {item}, it's already exsit!");
            }

            list.Add(item);
            return true;
        }

        public bool Remove(int item)
        {
            if (item > list.Count)
                throw new IndexOutOfRangeException($"{item} is bigger than list length");
            if (!list.Contains(item))
            {
                throw new ItemNotFoundException($"Connot found {item}");
            }
            list.Remove(item);
            return true;
        }

        public int Peek(int index)
        {
            if (index > list.Count)
                throw new IndexOutOfRangeException($"{index} is bigger than list length");
            return list[index];
        }

        public int this[int index]
        {
            get
            {
                if (index > list.Count)
                    throw new IndexOutOfRangeException($"{index} is bigger than list length");
                return this.list[index];
            }
            set
            {
                if (index > list.Count)
                    throw new IndexOutOfRangeException($"{index} is bigger than list length");
                if (list[index] == value)
                    return;
                if (list.Contains(value))
                    throw new ItemAlreadyExsitException($"{value}, it's already exsit!");
                list[index] = value;
            }
        }
    }
}
